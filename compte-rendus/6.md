# Compte-Rendu de l'avant deernière séance

## Date Séance 25 mars 2021
Nous avons mis en place le service Netdata sur la VM centos où le service Samba tourne. 
Nous l'avons aussi installé sur la VM Windows Server qui possède le service Windows Active Directory.  
Nous avons fait un tableau de bord personnalisé avec Netdata.   
On a mis en place le service Borg sur la VM Windows Server pour faire la sauvegarde du Samba.   
